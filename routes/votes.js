var express = require('express');
var router = express.Router();
const db = require('../database');

router.get('/', function (req, res, next) {
    db.pool.query('SELECT * FROM votes', (error, results) => {
        if (error) {
            res.status(200).send(error.message);
            // throw error;
        } else {
            res.status(200).json(results.rows);
        }
    });
});

router.post('/', function (req, res, next) {
    const { id, vote } = req.body;
    db.pool.query('INSERT INTO votes (id, vote) VALUES ($1, $2)', [id, vote], (error, results) => {
        if (error) {
            res.status(200).send(error.message);
            // throw error;
        } else {
            res.status(201).send(`Votes added with ID: ${id}`)
        }
    })
});

router.put('/:id', function (req, res, next) {
    const id = req.params.id;
    const { vote } = req.body;
    db.pool.query(
        'UPDATE votes SET vote = $1 WHERE id = $2',
        [vote, id],
        (error, results) => {
            if (error) {
                res.status(200).send(error.message);
                // throw error
            } else {
                res.status(200).send(`Votes modified with ID: ${id}`);
            }
        }
    )
});

router.delete('/:id', function (req, res, next) {
    const id = req.params.id;
    db.pool.query(
        'DELETE FROM votes WHERE id = $1', [id],
        (error, results) => {
            if (error) {
                res.status(200).send(error.message);
                // throw error
            } else {
                res.status(200).send(`Votes deleted with ID: ${id}`);
            }
        }
    )
});

module.exports = router;
